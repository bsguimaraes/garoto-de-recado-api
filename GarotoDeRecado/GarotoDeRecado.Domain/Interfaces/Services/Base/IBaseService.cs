﻿using System.Collections.Generic;

namespace GarotoDeRecado.Domain.Interfaces.Services.Base
{
    public interface IBaseService<T> where T : class
    {
        ICollection<T> GetAll();
        T Get(int id);
        void Create(T obj);
        void Update(T obj);
        void Delete(int id);
        void Dispose();
    }
}
