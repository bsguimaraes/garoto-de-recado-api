﻿using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Domain.Interfaces.Services.Base;

namespace GarotoDeRecado.Domain.Interfaces.Services
{
    public interface IEnderecoService : IBaseService<Endereco>
    {
    }
}
