﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GarotoDeRecado.Domain.Interfaces.Repositories.Base
{
    public interface IBaseRepository<T> where T : class
    {
        ICollection<T> GetAll();
        T Get(int id);
        void Create(T obj);
        void Update(T obj);
        void Delete(int id);
        void Dispose();
    }
}
