﻿using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Domain.Interfaces.Repositories.Base;

namespace GarotoDeRecado.Domain.Interfaces.Repositories
{
    public interface ITipoServicoRepository : IBaseRepository<TipoServico>
    {
    }
}
