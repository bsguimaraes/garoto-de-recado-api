﻿using System.Collections.Generic;
using GarotoDeRecado.Domain.Entities.Base;

namespace GarotoDeRecado.Domain.Entities
{
    public class TipoServico: BaseEntity
    {
        public string Nome { get; set; }
        public virtual ICollection<Servico> Servicos { get; set; }
    }
}
