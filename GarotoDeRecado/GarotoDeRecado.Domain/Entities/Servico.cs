﻿using GarotoDeRecado.Domain.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GarotoDeRecado.Domain.Entities
{
    public class Servico: BaseEntity
    {
        public DateTime Data { get; set; }
        public DateTime DataInicio { get; set; }
        public TimeSpan HoraInicio { get; set; }
        public DateTime DataFim { get; set; }
        public TimeSpan HoraFim { get; set; }
        public decimal Valor { get; set; }
        //public int UsuarioId { get; set; }
        //public int GarotoId { get; set; }
        //public int TipoServicoId { get; set; }
        //public int LocalOrigemId { get; set; }
        //public int LocalDestinoId { get; set; }
        [ForeignKey("UsuarioId ")]
        public virtual Usuario Usuario { get; set; }
        [ForeignKey("GarotoId ")]
        public virtual Garoto Garoto { get; set; }
        [ForeignKey("TipoServicoId ")]
        public virtual TipoServico TipoServico { get; set; }
        
        [ForeignKey("LocalOrigemId ")]
        public virtual Local LocalOrigem { get; set; }

        [ForeignKey("LocalDestinoId ")]
        public virtual Local LocalDestino { get; set; }
    }
}
