﻿using System;
using System.Collections.Generic;
using GarotoDeRecado.Domain.Entities.Base;

namespace GarotoDeRecado.Domain.Entities
{
    public class Garoto: BaseEntity
    {
        public string Email { get; set; }
        public string Senha { get; set; }
        public string Nome { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Rg { get; set; }
        public string Cpf { get; set; }
        public byte[] Foto { get; set; }
        public bool Disponivel { get; set; }
        public DateTime DataCadastro { get; set; }
        public int EnderecoId { get; set; }
        public int DocumentoId { get; set; }
        public virtual Documento Documento { get; set; }
        public virtual Endereco Endereco { get; set; }
        public virtual ICollection<Servico> Servicos { get; set; }
    }
}
