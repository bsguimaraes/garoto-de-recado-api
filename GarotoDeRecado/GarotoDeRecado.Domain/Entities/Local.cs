﻿using System.Collections.Generic;
using GarotoDeRecado.Domain.Entities.Base;

namespace GarotoDeRecado.Domain.Entities
{
    public class Local : BaseEntity
    {
        public string Nome { get; set; }
        public int EnderecoId { get; set; }
        public virtual Endereco Endereco { get; set; }
        //public virtual ICollection<Servico> Servicos { get; set; }
        //public virtual ICollection<Servico> ServicosDestino { get; set; }
    }
}
