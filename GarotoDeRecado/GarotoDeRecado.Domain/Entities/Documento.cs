﻿using GarotoDeRecado.Domain.Entities.Base;

namespace GarotoDeRecado.Domain.Entities
{
    public class Documento: BaseEntity
    {
        public string Rg { get; set; }
        public string Cpf { get; set; }
        public byte[] FotoRg { get; set; }
        public byte[] FotoRgResponsavel { get; set; }
        public virtual Garoto Garoto{ get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
