﻿using GarotoDeRecado.Domain.Entities.Base;

namespace GarotoDeRecado.Domain.Entities
{
    public class Endereco: BaseEntity
    {
        public string Logradouro { get; set; }
        public int Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Uf { get; set; }
        public string Cep { get; set; }

        public virtual Local Local { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual Garoto Garoto  { get; set; }

    }
}
