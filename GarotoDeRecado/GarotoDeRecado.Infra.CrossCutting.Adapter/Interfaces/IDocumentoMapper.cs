﻿using GarotoDeRecado.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace GarotoDeRecado.Infra.CrossCutting.Adapter.Interfaces
{
    public interface IDocumentoMapper
    {
        Documento MapperToEntity();
        //Documento MapperToEntity();
    }
}
