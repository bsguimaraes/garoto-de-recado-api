﻿using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Infra.Data.Configuration;
using Microsoft.EntityFrameworkCore;

namespace GarotoDeRecado.Infra.Data.Context
{
    public class GarotoContext : DbContext
    {
        public GarotoContext()
        {}

        public GarotoContext(DbContextOptions<GarotoContext> options) :base(options){}

        public DbSet<Documento> Documento { get; set; }
        public DbSet<Endereco> Endereco { get; set; }
        public DbSet<Garoto> Garoto { get; set; }
        public DbSet<Local> Local { get; set; }
        public DbSet<Servico> Servico { get; set; }
        public DbSet<TipoServico> TipoServico { get; set; }
        public DbSet<Usuario> Usuario { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DocumentoConfiguration());
            modelBuilder.ApplyConfiguration(new EnderecoConfiguration());
            modelBuilder.ApplyConfiguration(new GarotoConfiguration());
            modelBuilder.ApplyConfiguration(new LocalConfiguration());
            modelBuilder.ApplyConfiguration(new ServicoConfiguration());
            modelBuilder.ApplyConfiguration(new TipoServicoConfiguration());
            modelBuilder.ApplyConfiguration(new UsuarioConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
