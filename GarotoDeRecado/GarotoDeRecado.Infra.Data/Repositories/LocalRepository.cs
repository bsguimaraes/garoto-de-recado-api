﻿using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Domain.Interfaces.Repositories;
using GarotoDeRecado.Infra.Data.Context;
using GarotoDeRecado.Infra.Data.Repositories.Base;

namespace GarotoDeRecado.Infra.Data.Repositories
{
    public class LocalRepository : BaseRepository<Local>, ILocalRepository
    {
        public LocalRepository(GarotoContext context) : base(context)
        {
        }
    }
}
