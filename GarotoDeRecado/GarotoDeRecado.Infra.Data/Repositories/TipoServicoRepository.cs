﻿using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Domain.Interfaces.Repositories;
using GarotoDeRecado.Infra.Data.Context;
using GarotoDeRecado.Infra.Data.Repositories.Base;

namespace GarotoDeRecado.Infra.Data.Repositories
{
    public class TipoServicoRepository : BaseRepository<TipoServico>, ITipoServicoRepository
    {
        public TipoServicoRepository(GarotoContext context) : base(context)
        {
        }
    }
}
