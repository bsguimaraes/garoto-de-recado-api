﻿using System;
using System.Collections.Generic;
using System.Linq;
using GarotoDeRecado.Domain.Interfaces.Repositories.Base;
using GarotoDeRecado.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace GarotoDeRecado.Infra.Data.Repositories.Base
{
    public class BaseRepository<T> : IDisposable, IBaseRepository<T> where T : class
    {
        protected GarotoContext Db;

        public BaseRepository(GarotoContext context)
        {
            Db = context;
        }
        public void Update(T obj)
        {
            Db.Entry(obj).State = EntityState.Modified;
            Db.SaveChanges();
        }

        public void Dispose()
        {
            Db.Dispose();
        }

        public void Delete(int id)
        {
            var entity = Get(id);
            Db.Set<T>().Remove(entity);
            Db.SaveChanges();
        }

        public void Create(T obj)
        {
            Db.Set<T>().Add(obj);
            Db.SaveChanges();
        }

        public ICollection<T> GetAll()
        {
            return Db.Set<T>().AsNoTracking().ToList();
        }

        public T Get(int id)
        {
            return Db.Set<T>().Find(id);
        }
    }
}
