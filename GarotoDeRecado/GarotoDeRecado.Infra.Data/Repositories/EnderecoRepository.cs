﻿using System;
using System.Collections.Generic;
using System.Text;
using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Domain.Interfaces.Repositories;
using GarotoDeRecado.Infra.Data.Context;
using GarotoDeRecado.Infra.Data.Repositories.Base;

namespace GarotoDeRecado.Infra.Data.Repositories
{
    public class EnderecoRepository : BaseRepository<Endereco>, IEnderecoRepository
    {
        public EnderecoRepository(GarotoContext context) : base(context)
        {
        }
    }
}
