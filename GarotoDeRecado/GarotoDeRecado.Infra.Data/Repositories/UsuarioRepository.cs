﻿using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Domain.Interfaces.Repositories;
using GarotoDeRecado.Infra.Data.Context;
using GarotoDeRecado.Infra.Data.Repositories.Base;

namespace GarotoDeRecado.Infra.Data.Repositories
{
    public class UsuarioRepository : BaseRepository<Usuario>, IUsuarioRepository
    {
        public UsuarioRepository(GarotoContext context) : base(context)
        {

        }
    }
}
