﻿using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Domain.Interfaces.Repositories;
using GarotoDeRecado.Infra.Data.Context;
using GarotoDeRecado.Infra.Data.Repositories.Base;

namespace GarotoDeRecado.Infra.Data.Repositories
{
    public class ServicoRepository : BaseRepository<Servico>, IServicoRepository
    {
        public ServicoRepository(GarotoContext context) : base(context)
        {
        }
    }
}
