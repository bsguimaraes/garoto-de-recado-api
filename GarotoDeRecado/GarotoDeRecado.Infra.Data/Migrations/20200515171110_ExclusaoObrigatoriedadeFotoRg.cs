﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GarotoDeRecado.Infra.Data.Migrations
{
    public partial class ExclusaoObrigatoriedadeFotoRg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte[]>(
                name: "FotoRg",
                table: "Documento",
                nullable: true,
                oldClrType: typeof(byte[]),
                oldType: "varbinary(max)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte[]>(
                name: "FotoRg",
                table: "Documento",
                type: "varbinary(max)",
                nullable: false,
                oldClrType: typeof(byte[]),
                oldNullable: true);
        }
    }
}
