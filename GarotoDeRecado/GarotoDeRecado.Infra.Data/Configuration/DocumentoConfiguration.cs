﻿using GarotoDeRecado.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarotoDeRecado.Infra.Data.Configuration
{
    public class DocumentoConfiguration : IEntityTypeConfiguration<Documento>
    {
        public void Configure(EntityTypeBuilder<Documento> builder)
        {
            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.Garoto).WithOne(p => p.Documento);
        }
    }
}
