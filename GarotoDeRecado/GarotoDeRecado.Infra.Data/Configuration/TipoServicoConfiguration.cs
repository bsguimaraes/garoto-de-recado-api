﻿using System;
using GarotoDeRecado.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarotoDeRecado.Infra.Data.Configuration
{
    class TipoServicoConfiguration : IEntityTypeConfiguration<TipoServico>
    {
        public void Configure(EntityTypeBuilder<TipoServico> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Nome).IsRequired();

            builder.HasMany(p => p.Servicos).WithOne(p => p.TipoServico);
        }
    }
}
