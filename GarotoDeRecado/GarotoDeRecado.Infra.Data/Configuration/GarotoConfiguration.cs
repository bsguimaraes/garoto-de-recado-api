﻿using GarotoDeRecado.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarotoDeRecado.Infra.Data.Configuration
{
    public class GarotoConfiguration : IEntityTypeConfiguration<Garoto>
    {
        public void Configure(EntityTypeBuilder<Garoto> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Email).IsRequired();
            builder.Property(p => p.Senha).IsRequired();
            builder.Property(p => p.Rg).IsRequired();
            builder.Property(p => p.Cpf).IsRequired();
            builder.Property(p => p.Foto).IsRequired();
            builder.Property(p => p.Disponivel).IsRequired();
            builder.Property(p => p.DataNascimento).IsRequired();

            builder.HasMany(p => p.Servicos).WithOne(p => p.Garoto);
            builder.HasOne(p => p.Documento).WithOne(p => p.Garoto);
            builder.HasOne(p => p.Endereco).WithOne(p => p.Garoto);
        }
    }
}
