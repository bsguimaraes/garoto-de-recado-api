﻿using GarotoDeRecado.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarotoDeRecado.Infra.Data.Configuration
{
    public class EnderecoConfiguration : IEntityTypeConfiguration<Endereco>
    {
        public void Configure(EntityTypeBuilder<Endereco> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Logradouro).IsRequired();
            builder.Property(p => p.Numero).IsRequired();
            builder.Property(p => p.Bairro).IsRequired();
            builder.Property(p => p.Cidade).IsRequired();
            builder.Property(p => p.Uf).IsRequired();

            builder.HasOne(p => p.Usuario).WithOne(p => p.Endereco);
            builder.HasOne(p => p.Garoto).WithOne(p => p.Endereco);
        }
    }
}
