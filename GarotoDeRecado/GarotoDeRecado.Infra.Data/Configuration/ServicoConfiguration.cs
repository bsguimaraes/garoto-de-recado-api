﻿using System;
using GarotoDeRecado.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarotoDeRecado.Infra.Data.Configuration
{
    public class ServicoConfiguration :  IEntityTypeConfiguration<Servico>
    {
        public void Configure(EntityTypeBuilder<Servico> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Data).IsRequired();
            builder.Property(p => p.DataInicio).IsRequired();
            builder.Property(p => p.DataFim).IsRequired();
            //builder.Property(p => p.UsuarioId).IsRequired();
            //builder.Property(p => p.GarotoId).IsRequired();
            //builder.Property(p => p.LocalOrigemId).IsRequired();
            //builder.Property(p => p.LocalDestinoId).IsRequired();
            builder.Property(p => p.Valor).IsRequired();
            builder.Property(p => p.Valor).HasColumnType("decimal(8,2)");

            builder.HasOne(p => p.Usuario).WithMany(p => p.Servicos);
            builder.HasOne(p => p.Garoto).WithMany(p => p.Servicos);
            builder.HasOne(p => p.TipoServico).WithMany(p => p.Servicos);
            //builder.HasOne(p => p.LocalOrigem).WithMany(p => p.Servicos);
            //builder.HasOne(p => p.LocalDestino).WithMany(p => p.Servicos);

        }
    }
}
