﻿using System;
using GarotoDeRecado.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarotoDeRecado.Infra.Data.Configuration
{
    public class LocalConfiguration : IEntityTypeConfiguration<Local>
    {
        public void Configure(EntityTypeBuilder<Local> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Nome).IsRequired();

            builder.HasOne(p => p.Endereco).WithOne(p => p.Local);

            //builder.HasMany(p => p.ServicosOrigem)
            //       .WithOne(p => p.LocalOrigem)
            //       .HasForeignKey(p=>p.LocalOrigemId);
            
            //builder.HasMany(p => p.ServicosDestino)
            //       .WithOne(p => p.LocalDestino)
            //       .HasForeignKey(p => p.LocalDestinoId)
            //       .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
