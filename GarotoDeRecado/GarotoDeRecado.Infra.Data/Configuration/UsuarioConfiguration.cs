﻿using GarotoDeRecado.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarotoDeRecado.Infra.Data.Configuration
{
    public class UsuarioConfiguration : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.HasKey(p=>p.Id);

            builder.Property(p => p.Email).IsRequired();
            builder.Property(p => p.Senha).IsRequired();
            builder.Property(p => p.Nome).IsRequired();
            builder.Property(p => p.DataNascimento).IsRequired();
            builder.Property(p => p.Sexo).IsRequired();

            builder.HasMany(p => p.Servicos).WithOne(p => p.Usuario);   
            builder.HasOne(p => p.Documento).WithOne(p => p.Usuario);
            builder.HasOne(p => p.Endereco).WithOne(p => p.Usuario);
        }
    }
}
