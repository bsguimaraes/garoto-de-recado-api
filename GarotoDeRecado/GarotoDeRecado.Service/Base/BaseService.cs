﻿using System;
using System.Collections.Generic;
using System.Text;
using GarotoDeRecado.Domain.Interfaces.Repositories.Base;
using GarotoDeRecado.Domain.Interfaces.Services.Base;

namespace GarotoDeRecado.Service.Base
{
    public class BaseService<T> : IDisposable, IBaseService<T> where T : class
    {
        private readonly IBaseRepository<T> _repository;

        public BaseService(IBaseRepository<T> repository)
        {
            _repository = repository;
        }

        public void Update(T obj)
        {
            _repository.Update(obj);
        }

        public void Dispose()
        {
            _repository.Dispose();
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }

        public void Create(T obj)
        {
            _repository.Create(obj);
        }

        public ICollection<T> GetAll()
        {
            return _repository.GetAll();
        }

        public T Get(int id)
        {
            return _repository.Get(id);
        }
    }
}
