﻿using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Domain.Interfaces.Repositories;
using GarotoDeRecado.Domain.Interfaces.Services;
using GarotoDeRecado.Service.Base;

namespace GarotoDeRecado.Service
{
    public class ServicoService : BaseService<Servico>, IServicoService
    {
        private readonly IServicoRepository  _servicoRepository;
        public ServicoService(IServicoRepository servicoRepository) : base(servicoRepository)
        {
            _servicoRepository = servicoRepository;
        }
    }
}
