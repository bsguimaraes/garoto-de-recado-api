﻿using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Domain.Interfaces.Repositories;
using GarotoDeRecado.Domain.Interfaces.Services;
using GarotoDeRecado.Service.Base;

namespace GarotoDeRecado.Service
{
    public class LocalService : BaseService<Local>, ILocalService
    {
        private readonly ILocalRepository  _localRepository;
        public LocalService(ILocalRepository localRepository) : base(localRepository)
        {
            _localRepository = localRepository;
        }
    }
}
