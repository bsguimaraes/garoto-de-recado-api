﻿using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Domain.Interfaces.Repositories;
using GarotoDeRecado.Domain.Interfaces.Services;
using GarotoDeRecado.Domain.Interfaces.Services.Base;
using GarotoDeRecado.Service.Base;

namespace GarotoDeRecado.Service
{
    public class TipoServicoService : BaseService<TipoServico>, ITipoServicoService
    {
        private readonly ITipoServicoRepository _tipoServicoRepository ;
        public TipoServicoService(ITipoServicoRepository tipoServicoRepository) : base(tipoServicoRepository)
        {
            _tipoServicoRepository = tipoServicoRepository;
        }
    }
}
