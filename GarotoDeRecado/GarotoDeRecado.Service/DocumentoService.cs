﻿using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Domain.Interfaces.Repositories;
using GarotoDeRecado.Domain.Interfaces.Services;
using GarotoDeRecado.Domain.Interfaces.Services.Base;
using GarotoDeRecado.Service.Base;

namespace GarotoDeRecado.Service
{
    public class DocumentoService : BaseService<Documento>, IDocumentoService
    {
        private readonly IDocumentoRepository _documentoRepository;
        public DocumentoService(IDocumentoRepository documentoRepository) : base(documentoRepository)
        {
            _documentoRepository = documentoRepository;
        }
    }
}
