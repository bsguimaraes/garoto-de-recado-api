﻿using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Domain.Interfaces.Repositories;
using GarotoDeRecado.Domain.Interfaces.Services;
using GarotoDeRecado.Service.Base;

namespace GarotoDeRecado.Service
{
    public class GarotoService : BaseService<Garoto>, IGarotoService
    {
        private readonly IGarotoRepository _garotoRepository;
        public GarotoService(IGarotoRepository garotoRepository) : base(garotoRepository)
        {
            _garotoRepository = garotoRepository;
        }
    }
}
