﻿using GarotoDeRecado.Domain.Interfaces.Repositories;
using GarotoDeRecado.Domain.Interfaces.Repositories.Base;
using GarotoDeRecado.Domain.Interfaces.Services;
using GarotoDeRecado.Domain.Interfaces.Services.Base;
using GarotoDeRecado.Infra.Data.Context;
using GarotoDeRecado.Infra.Data.Repositories;
using GarotoDeRecado.Infra.Data.Repositories.Base;
using GarotoDeRecado.Service;
using GarotoDeRecado.Service.Base;
using Microsoft.Extensions.DependencyInjection;

namespace GarotoDeRecado.Infra.CrossCutting.IoC
{
    public static class DIContainer
    {
        public static void RegisterDependencies(IServiceCollection services)
        {
            //services.AddScoped<GarotoContext>();

            //services.AddScoped(typeof(IBaseService<>), typeof(BaseService<>));
            services.AddScoped<IDocumentoService, DocumentoService>();
            services.AddScoped<IEnderecoService, EnderecoService>();
            services.AddScoped<IGarotoService, GarotoService>();
            services.AddScoped<ILocalService, LocalService>();
            services.AddScoped<IServicoService, ServicoService>();
            services.AddScoped<ITipoServicoService, TipoServicoService>();
            services.AddScoped<IUsuarioService, UsuarioService>();

            //services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddScoped<IDocumentoRepository, DocumentoRepository>();
            services.AddScoped<IEnderecoRepository, EnderecoRepository>();
            services.AddScoped<IGarotoRepository, GarotoRepository>();
            services.AddScoped<ILocalRepository, LocalRepository>();
            services.AddScoped<IServicoRepository, ServicoRepository>();
            services.AddScoped<ITipoServicoRepository, TipoServicoRepository>();
            services.AddScoped<IUsuarioRepository, UsuarioRepository>();
        }
    }
}
