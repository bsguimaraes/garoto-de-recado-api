﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GarotoDeRecado.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EnderecoController : ControllerBase
    {
        private readonly IEnderecoService _endereco;

        public EnderecoController(IEnderecoService endereco)
        {
            _endereco = endereco;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Endereco>> Get()
        {
            var endereco = _endereco.GetAll();

            return Ok(endereco);
        }

        [HttpGet("{id}")]
        public ActionResult<Endereco> Get([FromRoute] int id)
        {
            var endereco = _endereco.Get(id);

            if (endereco == null) return NotFound();

            return endereco;
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, Endereco endereco)
        {
            if (id != endereco.Id)
            {
                return BadRequest();
            }

            try
            {
                _endereco.Update(endereco);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EnderecoExiste(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public ActionResult<Servico> Post(Endereco endereco)
        {
            try
            {
                if (endereco == null) return BadRequest();

                _endereco.Create(endereco);

                return Ok("Endereço inserido com sucesso!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult<Endereco> Delete(int id)
        {
            try
            {
                _endereco.Delete(id);

                return Ok("Exclusão realizada com sucesso!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private bool EnderecoExiste(int id)
        {
            return (_endereco.Get(id) != null);

        }
    }
}