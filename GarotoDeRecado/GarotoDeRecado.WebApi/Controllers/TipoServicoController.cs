﻿using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace GarotoDeRecado.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoServicoController : ControllerBase
    {
        private readonly ITipoServicoService _tipoServicoService;

        public TipoServicoController(ITipoServicoService tipoServicoService)
        {
            _tipoServicoService = tipoServicoService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TipoServico>> Get()
        {
            var tipoServicos = _tipoServicoService.GetAll();

            return Ok(tipoServicos);
        }

        [HttpGet("{id}")]
        public ActionResult<TipoServico> Get([FromRoute] int id)
        {
            var tipoServico = _tipoServicoService.Get(id);

            if (tipoServico == null) return NotFound();

            return tipoServico;
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, TipoServico tipoServico)
        {
            if (id != tipoServico.Id)
            {
                return BadRequest();
            }

            try
            {
                _tipoServicoService.Update(tipoServico);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoServicoExiste(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public ActionResult<Servico> Post(TipoServico tipoServico)
        {
            try
            {
                if (tipoServico == null) return BadRequest();

                _tipoServicoService.Create(tipoServico);

                return Ok("Tipo de Serviço inserido com sucesso!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult<TipoServico> Delete(int id)
        {
            try
            {
                _tipoServicoService.Delete(id);

                return Ok("Exclusão realizada com sucesso!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private bool TipoServicoExiste(int id)
        {
            return (_tipoServicoService.Get(id) != null);

        }
    }
}