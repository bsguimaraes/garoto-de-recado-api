﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Infra.Data.Context;
using GarotoDeRecado.Domain.Interfaces.Services;

namespace GarotoDeRecado.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentoController : ControllerBase
    {
        private readonly IDocumentoService _documentoService;

        public DocumentoController(IDocumentoService documentoService)
        {
            _documentoService = documentoService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Documento>> Get()
        {
            var documentos = _documentoService.GetAll();

            return Ok(documentos);
        }

        [HttpGet("{id}")]
        public ActionResult<Documento> Get([FromRoute] int id)
        {
            var documento = _documentoService.Get(id);

            if (documento == null) return NotFound();

            return documento;
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, Documento documento)
        {
            if (id != documento.Id)
            {
                return BadRequest();
            }

            try
            {
                _documentoService.Update(documento);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DocumentoExiste(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public ActionResult<Documento> Post([FromBody] Documento documento)
        {
            try
            {
                if (documento == null) return BadRequest();

                _documentoService.Create(documento);

                return Ok("Documento inserido com sucesso!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult<Documento> Delete(int id)
        {
            try
            {
                _documentoService.Delete(id);

                return Ok("Exclusão realizada com sucesso!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private bool DocumentoExiste(int id)
        {
            return (_documentoService.Get(id) != null);
        }
    }
}
