﻿using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace GarotoDeRecado.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GarotoController : ControllerBase
    {
        private readonly IGarotoService _garotoService;

        public GarotoController(IGarotoService garotoService)
        {
            _garotoService = garotoService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Garoto>> Get()
        {
            var garotos = _garotoService.GetAll();

            return Ok(garotos);
        }

        [HttpGet("{id}")]
        public ActionResult<Garoto> Get([FromRoute] int id)
        {
            var garoto = _garotoService.Get(id);

            if (garoto == null) return NotFound();

            return garoto;
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, Garoto garoto)
        {
            if (id != garoto.Id)
            {
                return BadRequest();
            }

            try
            {
                _garotoService.Update(garoto);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GarotoExiste(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public ActionResult<Garoto> Post(Garoto garoto)
        {
            try
            {
                if (garoto == null) return BadRequest();

                _garotoService.Create(garoto);

                return Ok("Garoto inserido com sucesso!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult<Garoto> Delete(int id)
        {
            try
            {
                _garotoService.Delete(id);

                return Ok("Exclusão realizada com sucesso!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private bool GarotoExiste(int id)
        {
            return (_garotoService.Get(id) != null);

        }
    }
}