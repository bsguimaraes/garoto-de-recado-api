﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GarotoDeRecado.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServicoController : ControllerBase
    {
        private readonly IServicoService _servicoService;
        
        public ServicoController(IServicoService servicoService)
        {
            _servicoService = servicoService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Servico>> Get()
        {
            var servicos = _servicoService.GetAll();

            return Ok(servicos);
        }

        [HttpGet("{id}")]
        public ActionResult<Servico> Get([FromRoute] int id)
        {
            var servico = _servicoService.Get(id);

            if (servico == null) return NotFound();

            return servico;
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, Servico servico)
        {
            if (id != servico.Id)
            {
                return BadRequest();
            }

            try
            {
                _servicoService.Update(servico);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServicoExiste(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public ActionResult<Servico> Post(Servico servico)
        {
            try
            {
                if (servico == null) return BadRequest();

                _servicoService.Create(servico);

                return Ok("Serviço inserido com sucesso!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult<Servico> Delete(int id)
        {
            try
            {
                _servicoService.Delete(id);

                return Ok("Exclusão realizada com sucesso!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private bool ServicoExiste(int id)
        {
            return (_servicoService.Get(id) != null);

        }
    }
}