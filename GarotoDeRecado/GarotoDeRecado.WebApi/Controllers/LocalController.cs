﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GarotoDeRecado.Domain.Entities;
using GarotoDeRecado.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GarotoDeRecado.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocalController : ControllerBase
    {
        private readonly ILocalService _localService;

        public LocalController(ILocalService LocalService)
        {
            _localService = LocalService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Local>> Get()
        {
            var locais = _localService.GetAll();

            return Ok(locais);
        }

        [HttpGet("{id}")]
        public ActionResult<Local> Get([FromRoute] int id)
        {
            var local = _localService.Get(id);

            if (local == null) return NotFound();

            return local;
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, Local local)
        {
            if (id != local.Id)
            {
                return BadRequest();
            }

            try
            {
                _localService.Update(local);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LocalExiste(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public ActionResult<Garoto> Post(Local local)
        {
            try
            {
                if (local == null) return BadRequest();

                _localService.Create(local);

                return Ok("Local inserido com sucesso!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult<Local> Delete(int id)
        {
            try
            {
                _localService.Delete(id);

                return Ok("Exclusão realizada com sucesso!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private bool LocalExiste(int id)
        {
            return (_localService.Get(id) != null);

        }
    }
}