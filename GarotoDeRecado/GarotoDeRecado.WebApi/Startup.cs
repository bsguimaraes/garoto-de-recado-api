using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GarotoDeRecado.Domain.Interfaces.Repositories;
using GarotoDeRecado.Domain.Interfaces.Repositories.Base;
using GarotoDeRecado.Domain.Interfaces.Services;
using GarotoDeRecado.Domain.Interfaces.Services.Base;
using GarotoDeRecado.Infra.CrossCutting.IoC;
using GarotoDeRecado.Infra.Data.Context;
using GarotoDeRecado.Infra.Data.Repositories;
using GarotoDeRecado.Infra.Data.Repositories.Base;
using GarotoDeRecado.Service;
using GarotoDeRecado.Service.Base;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace GarotoDeRecado.WebApi
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var connection = Configuration.GetConnectionString("DefaultConnection");

            services.AddDbContext<GarotoContext>(options => options.UseSqlServer(connection));
            services.AddMemoryCache();
            services.AddControllers().SetCompatibilityVersion(CompatibilityVersion.Version_2_0);
            DIContainer.RegisterDependencies(services);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "API - Garoto de Recado",
                    Description = "API do aplicativo Garoto de Recado",
                    Contact = new OpenApiContact
                    {
                        Name = "Bruno Guimar�es",
                        Email = "brunos.guimaraes@outlook.com"
                    },
                });
            });

            services.AddCors(options =>
            {
                options.AddPolicy("DefaultPolicy", builder =>
                {
                    builder.AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader();
                });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API - Garoto de Recado");
                c.RoutePrefix = string.Empty;
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseCors("DefaultPolicy");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
